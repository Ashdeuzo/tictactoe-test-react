import firebase from 'firebase';

const config = {
	apiKey: '### FIREBASE API KEY ###',
	authDomain: '### FIREBASE AUTH DOMAIN ###',
	projectId: '### CLOUD FIRESTORE PROJECT ID ###',
};

firebase.initializeApp(config);

export const db = firebase.firestore();
